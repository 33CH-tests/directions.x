/*! \file  clockwise.c
 *
 *  \brief
 *
 *  \author jjmcd
 *  \date November 21, 2018, 2:41 PM
 *
 * Software License Agreement
 * Copyright (c) 2018 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "../include/graphicsHostLibrary.h"
#include "directions.h"

/*! clockwise - */
/*!
 *
 */
void clockwise( int x, int y, int scale )
{
  TFTcircle(x,y,80);
  TFTcircle(x,y,60);
  TFTline(x+5,y+78,x-5,y+70);
  TFTline(x-5,y+70,x+5,y+62);
  TFTline(x-5,y-78,x+5,y-70);
  TFTline(x+5,y-70,x-5,y-62);
  TFTline(x-78,y+5,x-70,y-5);
  TFTline(x-70,y-5,x-62,y+5);
  TFTline(x+78,y-5,x+70,y+5);
  TFTline(x+70,y+5,x+62,y-5);
  idleSync();
}
