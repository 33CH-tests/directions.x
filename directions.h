/*! \file  directions.h
 *
 *  \brief Constants and function prototypes for directions
 *
 *  \author jjmcd
 *  \date November 21, 2018, 2:37 PM
 *
 * Software License Agreement
 * Copyright (c) 2018 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#ifndef DIRECTIONS_H
#define	DIRECTIONS_H

#ifdef	__cplusplus
extern "C"
{
#endif

#define scaledline(x1,y1,x2,y2) { TFTline(x+scale*x1,y+scale*y1,x+scale*x2,y+scale*y2); }

void counterClockwise( int, int, int );
void clockwise( int, int, int );
void downArrow( int, int, int );
void upArrow( int, int, int );
void leftArrow( int, int, int );
void rightArrow( int, int, int );

#ifdef	__cplusplus
}
#endif

#endif	/* DIRECTIONS_H */

