/*! \file  downArrow.c
 *
 *  \brief
 *
 *  \author jjmcd
 *  \date November 21, 2018, 2:43 PM
 *
 * Software License Agreement
 * Copyright (c) 2018 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "../include/graphicsHostLibrary.h"
#include "directions.h"

/*! downArrow - */
/*!
 *
 */
void downArrow( int x, int y, int scale )
{
  scaledline(2,0,4,0);
  scaledline(4,0,4,5);
  scaledline(4,5,6,5);
  scaledline(6,5,3,8);
  scaledline(3,8,0,5);
  scaledline(0,5,2,5);
  scaledline(2,5,2,0);
  idleSync();
}
