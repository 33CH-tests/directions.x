/*! \file  leftArrow.c
 *
 *  \brief
 *
 *  \author jjmcd
 *  \date November 21, 2018, 2:50 PM
 *
 * Software License Agreement
 * Copyright (c) 2018 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "../include/graphicsHostLibrary.h"
#include "directions.h"

/*! leftArrow - */
/*!
 *
 */
void leftArrow(int x, int y, int scale )
{
  scaledline(0,3,3,0);
  scaledline(3,0,3,2);
  scaledline(3,2,8,2);
  scaledline(8,2,8,4);
  scaledline(8,4,3,4);
  scaledline(3,4,3,6);
  scaledline(3,6,0,3);
  idleSync();
}
