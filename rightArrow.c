/*! \file  rightArrow.c
 *
 *  \brief
 *
 *  \author jjmcd
 *  \date November 21, 2018, 2:50 PM
 *
 * Software License Agreement
 * Copyright (c) 2018 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "../include/graphicsHostLibrary.h"
#include "directions.h"

/*! rightArrow - */
/*!
 *
 */
void rightArrow(int x, int y, int scale)
{
  scaledline(0, 2, 5, 2);
  scaledline(5, 2, 5, 0);
  scaledline(5, 0, 8, 3);
  scaledline(8, 3, 5, 6);
  scaledline(5, 6, 5, 4);
  scaledline(5, 4, 0, 4);
  scaledline(0, 4, 0, 2);
  idleSync();
}
