/*! \file  upArrow.c
 *
 *  \brief
 *
 *  \author jjmcd
 *  \date November 21, 2018, 2:45 PM
 *
 * Software License Agreement
 * Copyright (c) 2018 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "../include/graphicsHostLibrary.h"
#include "directions.h"

/*! upArrow - */
/*!
 *
 */
void upArrow( int x, int y, int scale )
{
  scaledline(3,0,6,3);
  scaledline(6,3,4,3);
  scaledline(4,3,4,8);
  scaledline(4,8,2,8);
  scaledline(2,8,2,3);
  scaledline(2,3,0,3);
  scaledline(0,3,3,0);
  idleSync();
}
